#!/usr/bin/python
#
# Permet de récupérer les photos des députés et des sénateurs (avec 200px de hauteur)
# via l'API de Regards Citoyens : https://github.com/regardscitoyens/nosdeputes.fr/blob/master/doc/api.md
#
# usage : python download_photos.py [FICHIER_CSV] [TYPE] [REPERTOIRE_DESTINATION]
# exemple députés : python download_photos.py ../data/fichier_deputes_commissions.csv depute ../data/photos_dep/
# exemple sénateurs : python download_photos.py ../data/fichier_senateurs_commissions.csv senateur ../data/photos_dep/
#
 
import csv
import urllib.request
import sys
 
fname = sys.argv[1]
type = sys.argv[2]
dest_folder = sys.argv[3]

file = open(fname, "r")
 
try:
    reader = csv.reader(file)
    next(reader)
    for row in reader:
        slug = row[1]
        urllib.request.urlretrieve('https://www.nos'+type+'s.fr/'+type+'/photo/'+slug+'/200', dest_folder+slug+'.png')
finally:
    file.close()
