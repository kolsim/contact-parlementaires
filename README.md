# Contact parlementaires

Page qui met à disposition les coordonnées des parlementaires (députés et sénateurs) avec possibilité de les filtrer par groupe / département / commission.

Ceci dans le but de les interpeler par rapport au projet de loi d'interdiction de l'Instruction en Famille (IEF) (Pour avoir du contexte, voir par exemple [la lettre ouverte au gouvernement](http://www.lesenfantsdabord.org/lettre-ouverte-conjointe-a/) adressée par plusieurs associations)

## Inspiration
Merci à [La Quadrature du Net](https://www.laquadrature.net/) qui a inspiré cette page, et qui a permis de gagner beaucoup de temps car son code source est partagé (https://git.laquadrature.net/Oncela5eva/backphone) et a donc servi de base à ce projet.

## Sources des données
Les données concernant les députés et les sénateurs viennent de plusieurs sources :
- fichiers csv fournis par [VoxPublic](https://www.voxpublic.org) :
  - https://www.voxpublic.org/spip.php?page=annuaire&cat=deputes
  - https://www.voxpublic.org/spip.php?page=annuaire&cat=senateurs


- données mises à disposition par l'association Regards Citoyens via leur API :
  - https://github.com/regardscitoyens/nosdeputes.fr/blob/master/doc/api.md


## Étapes pour avoir toutes les données nécessaires

### Récupérer les fichiers csv sur VoxPublic
cf. urls ci-dessus

### MAJ des données
- Supprimer les colonnes inutiles (cf. fichiers csv de ce dépôt), rajouter une colonne `slug` qui permettra de récupérer les photos (la remplir à l'aide des fichiers csv de Regards Citoyens)


- Ajouter de la colonne `url_officielle` (à partir des fichiers csv de Regards Citoyens)


- MAJ des informations dans la colonne `commission_permanente` :
  - Mettre "Sans commission" pour les parlementaires qui n'ont pas de commission
  - Modifier les noms de certaines commissions dans le fichier des sénateurs (cf. fichier csv de ce dépôt)


- Ajout / MAJ de données (tel / mail / ..)


### Récupération des photos
Les photos sont récupérées automatiquement grâce au script python [download_photos.py](https://framagit.org/coopli/maintien-ief/-/blob/master/outils/download_photos.py)

Usage :  
`python download_photos.py [FICHIER_CSV] [TYPE] [REPERTOIRE_DESTINATION]`

Exemple députés :  
`python download_photos.py ../data/fichier_deputes_commissions.csv depute ../data/photos_dep/`

Exemple sénateurs :  
`python download_photos.py ../data/fichier_senateurs_commissions.csv senateur ../data/photos_dep/`

### Génération des fichiers json
Il faut générer un fichier json à partir de chaque fichier csv.

Ici, [csvtojson](https://github.com/Keyang/node-csvtojson/) a été utilisé


## Licence

Les fichiers de données sont en OpenData sous [licence ODbL](https://opendatacommons.org/licenses/odbl/).

Le code source est sous licence [GPL v3](https://www.gnu.org/licenses/gpl-3.0.html)
