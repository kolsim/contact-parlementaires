
// Liste des parlementaires
var deputes = [];
var senateurs = [];

// Valeur par défaut du groupe, département et commission selectionnés
var groupe_dep = 0;
var groupe_sen = 0;
var commission_dep = 0;
var commission_sen = 0;
var circo_dep = 0;
var circo_sen = 0;

// Parlementaires sélectionnés
var selected_dep = [];
var current_dep = 0;
var selected_sen = [];
var current_sen = 0;

// L'adresse des JSON contenant les informations sur les parlementaires
var url_dep = 'data/deputes.json';
var url_sen = 'data/senateurs.json';

// Récupère les informations sur les députés et les sénateurs
onload = function ()
{
    var server = new XMLHttpRequest();
    server.open("GET", url_dep, true);
    server.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    server.send();

    server.onreadystatechange = function()
    {
        if (this.readyState == XMLHttpRequest.DONE && this.status == 200)
        {
            deputes = JSON.parse(server.responseText);
            construct(deputes, 'dep');
        }
    }

    var server2 = new XMLHttpRequest();
    server2.open("GET", url_sen, true);
    server2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    server2.send();

    server2.onreadystatechange = function()
    {
        
        if (this.readyState == XMLHttpRequest.DONE && this.status == 200)
        {
            senateurs = JSON.parse(server2.responseText);
            construct(senateurs, 'sen');
        }
    }

    // on attache les événements aux onglets et au boutons de la page
    document.getElementById("dep_show").onclick = showDeputes;
    document.getElementById("sen_show").onclick = showSenateurs;
    document.getElementById("dep_next").onclick = nextDep;
    document.getElementById("sen_next").onclick = nextSen;
}

// Construit le formulaire à partir des informations récupérées
function construct(parlementaires, type)
{
    // Groupes, commissions, et circo possibles
    var groupes = [];
    var commissions = [];
    var circos = [];


    // Récupère les noms de groupe, de circo et les commissions
    for (var i = 0; i < parlementaires.length; i++)
    {
        if (circos.indexOf(parlementaires[i].nom_circo) == -1)
            circos.push(parlementaires[i].nom_circo);
        if (groupes.indexOf(parlementaires[i].groupe_sigle) == -1)
            groupes.push(parlementaires[i].groupe_sigle);
        if (commissions.indexOf(parlementaires[i].commission_permanente) == -1)
            commissions.push(parlementaires[i].commission_permanente);
    }

    // Remplit le sélecteur de groupe
    groupes.sort();
    for (var i = 0; i < groupes.length; i++)
    {
        var option = document.createElement("option");
            option.value = groupes[i];
            option.innerHTML = printGroupe(groupes[i]);
            document.getElementById(type+"_select_groupe").appendChild(option);
    }

    // Gère la sélection de groupe dans le formulaire
    document.getElementById(type+"_select_groupe").onchange = function()
    {
        window['groupe_'+type] = this.value;
        filter(type);
    }

    // Remplit le sélecteur de circo
    circos.sort();
    for (var i = 0; i < circos.length; i++)
    {
        var option = document.createElement("option");
            option.value = circos[i];
            option.innerHTML = circos[i];
            document.getElementById(type+"_select_circo").appendChild(option);
    }

    // Gère la sélection de crico dans le formulaire
    document.getElementById(type+"_select_circo").onchange = function()
    {
        window['circo_'+type] = this.value;
        filter(type);
    }

    // Remplit le sélecteur de commissions
    commissions.sort();
    for (var i = 0; i < commissions.length; i++)
    {
        var option = document.createElement("option");
            option.value = commissions[i];
            option.innerHTML = commissions[i];
            document.getElementById(type+"_select_commission").appendChild(option);
    }

    // Gère la sélection de crico dans le formulaire
    document.getElementById(type+"_select_commission").onchange = function()
    {
        window['commission_'+type] = this.value;
        filter(type);
    }

    // Lance le premier filtre
    filter("dep");
}

// Filtre les parlementaires à afficher en fonction du groupe, de la circo et de la commission
function filter(type)
{
    var array_name ='';
    if (type == 'dep')
        array_name = 'deputes';
    else if (type == 'sen')
        array_name = 'senateurs';

    var commission_speciale = 0;
    if (document.getElementById("dep-com-speciale_show").classList.contains("actif"))
        commission_speciale = 1;

    // Récupère la liste de députés correspondant au filtre
    // window[var] permet d'appeler une variable globale dont le nom dépend d'une variable "var".
    // Ici il s'agit de pouvoir appeler deputes[] ou senateurs[] selon le paramètre donné à la fonction "filter"
    var selected_parlementaires = [];

    for (var i = 0; i < window[array_name].length; i++)
    {
        if ( (window['groupe_'+type] == 0 || window[array_name][i].groupe_sigle == window['groupe_'+type])
        &&   (window['circo_'+type] == 0 || window[array_name][i].nom_circo == window['circo_'+type]) 
        &&   ( (commission_speciale && window[array_name][i].commission_speciale == 1) 
               || (!commission_speciale && (window['commission_'+type] == 0 || window[array_name][i].commission_permanente == window['commission_'+type]))
             ) )
        {
            selected_parlementaires.push(window[array_name][i]);
        }
    }

    // Met à jour le nombre de parlementaires indiqués dans le filtre
    document.getElementById(type+"_total").innerHTML = selected_parlementaires.length;

    // Choisit un parlementaire au hasard dans le groupe
    window['current_'+type] = Math.floor(Math.random()*selected_parlementaires.length);

    if (type == 'dep' && selected_parlementaires.length)
    {
        selected_dep = selected_parlementaires;
        document.getElementById("dep_info").style.display = "block";
        document.getElementById("dep_next").style.display = "block";
        nextDep();
    }
    else if (type == 'sen' && selected_parlementaires.length)
    {
        selected_sen = selected_parlementaires;
        document.getElementById("sen_info").style.display = "block";
        document.getElementById("sen_next").style.display = "block";
        nextSen();
    }
    // La liste est vide, on masque les info sur le parlementaire
    else
    {
        document.getElementById(type+"_photo").style.backgroundImage = "url()";
        document.getElementById(type+"_info").style.display = "none";
        document.getElementById(type+"_next").style.display = "none";
        if (type == 'dep' && commission_speciale)
            document.getElementById("dep_contact").classList.add("special");
        else
            document.getElementById("dep_contact").classList.remove("special");
    }
}

// Retourne le nom complet du groupe 
function printGroupe(groupe)
{
    switch(groupe) {
        case "AE":
            return "Agir Ensemble";
            break;
        case "CRCE":
            return "Communiste républicain citoyen et Écologiste";
            break;
        case "CRC":
            return "Communiste républicain et citoyen";
            break;
        case "CRC-SPG":
            return "Communiste, Républicain, Citoyen et des Sénateurs du Parti de Gauche";
            break;
        case "ECO":
            return "Écologiste";
            break;
        case "EST":
            return "Écologiste - Solidarité et Territoires";
            break;
        case "GDR":
            return "Gauche Démocrate et Républicaine";
            break;
        case "LFI":
            return "La France Insoumise";
            break;
        case "LR":
            return "Les Républicains";
            break;
        case "LREM":
            return "La République En Marche";
            break;
        case "LT":
            return "Libertés et Territoires";
            break;
        case "MODEM":
            return "Mouvement Démocrate et Démocrates apparentés";
            break;
        case "NI":
            return "Non Inscrits";
            break;
        case "RDPI":
            return "Rassemblement des démocrates, progressistes et indépendants";
            break;
        case "RDSE":
            return "Rassemblement Démocratique et Social européen";
            break;
        case "RTLI":
            return "Les Indépendants - République et Territoires";
            break;
        case "SER":
            return "Socialiste, Écologiste et Républicain";
            break;
        case "SOC":
            return "Socialistes et apparentés";
            break;
        case "SOCV":
            return "Socialiste";
            break;
        case "UC":
            return "Union Centriste";
            break;
        case "UCR":
            return "Union Centriste et Républicaine";
            break;     
        case "UDI":
            return "Union des Démocrates et Indépendants";
            break;
    }
}

// Affiche le téléphone en enlevant le "33" au début et en mettant un espace tous les deux chiffres
function printTel(tel)
{
    return tel.replace(/^33/,0).replace(/(.{2})(?=.)/g,"$1 ");
}

// Affiche le député suivant
function nextDep()
{
    current_dep++;
    if (current_dep == selected_dep.length)
        current_dep = 0;

    // Met à jour les informations à afficher
    update('dep');
}

// Affiche le sénateur suivant
function nextSen()
{
    current_sen++;
    if (current_sen == selected_sen.length)
        current_sen = 0;

    // Met à jour les informations à afficher
    update('sen');
}

// permet d'afficher l'onglet députés de la commission spéciale
function showDeputesComSpeciale()
{
    document.getElementById("dep-com-speciale_show").classList.add("actif");
    document.getElementById("dep_show").classList.remove("actif");
    document.getElementById("sen_show").classList.remove("actif");
    document.getElementById("dep_contact").style.display = "block";
    document.getElementById("sen_contact").style.display = "none";
    filter("dep");
}

// permet d'afficher l'onglet députés
function showDeputes()
{
    document.getElementById("dep-com-speciale_show").classList.remove("actif");
    document.getElementById("dep_show").classList.add("actif");
    document.getElementById("sen_show").classList.remove("actif");
    document.getElementById("dep_contact").style.display = "block";
    document.getElementById("sen_contact").style.display = "none";
    filter("dep");
}

// permet d'afficher l'onglet sénateurs
function showSenateurs()
{
    document.getElementById("dep-com-speciale_show").classList.remove("actif");
    document.getElementById("dep_show").classList.remove("actif");
    document.getElementById("sen_show").classList.add("actif");
    document.getElementById("dep_contact").style.display = "none";
    document.getElementById("sen_contact").style.display = "block";
    filter("sen");
}



// Met à jour les informations à afficher
function update(type)
{
    var item_number = window['current_'+type];
    var selected = window['selected_'+type];
    // item correspond au député ou au sénateur affiché
    var item = selected[item_number];

    var mails = item.emails.split("|");
    var tels = item.tel.split("|");
    
    // on est dans le cas d'un député qui est dans la commission spéciale
    if (type == "dep" && item.commission_speciale == 1)
    {
        document.getElementById("dep_contact").classList.add("special");
        document.getElementById("dep_commission-speciale").style.display = "inline-block";

    }
    else
    {
        document.getElementById("dep_contact").classList.remove("special");
        document.getElementById("dep_commission-speciale").style.display = "none";
    }

    // on est dans le cas de l'onglet "Députés commission spéciale" qui est activé : on ne veut pas afficher le sélecteur de commissions
    if (document.getElementById("dep-com-speciale_show").classList.contains("actif"))
    {
        document.getElementById("dep_select_commission").style.display = "none";
        document.getElementById("commission-speciale").style.display = "inline-block";
    }
    else
    {
        document.getElementById("dep_select_commission").style.display = "inline-block";
        document.getElementById("commission-speciale").style.display = "none";
    }
    
    document.getElementById(type+"_nom").innerHTML = item.nom+' <a href="'+item.url_officielle+'" target="_blank" title="Voir sa fiche sur le site de l\'assemblée / du sénat"></a>';
    document.getElementById(type+"_photo").style.backgroundImage = "url('data/photos_"+type+"/"+item.slug+".png')";
    document.getElementById(type+"_groupe").innerHTML = printGroupe(item.groupe_sigle);
    document.getElementById(type+"_circo").innerHTML = item.nom_circo;
    document.getElementById(type+"_commission").innerHTML = item.fonction+" de la commission "+item.commission_permanente;

    // Il peut y avoir jusqu'à 2 numéros de téléphone
    for (var i = 0; i < 2; i++) 
    {
        if (tels[i] !== undefined)
        {
            document.getElementById(type+"_tel"+i).href = "tel:+" + tels[i];
            document.getElementById(type+"_tel"+i).innerHTML = printTel(tels[i]);
            document.getElementById(type+"_tel"+i).style.display = "block";
        }
        else
        {
            document.getElementById(type+"_tel"+i).href = "";
            document.getElementById(type+"_tel"+i).innerHTML = "";
            document.getElementById(type+"_tel"+i).style.display = "none";
        }
    }  
    
    // Il peut y avoir jusqu'à 3 adresses email
    for (var i = 0; i < 3; i++) 
    {
        if (mails[i] !== undefined)
        {
            document.getElementById(type+"_mail"+i).href = "mailto:" + mails[i];
            document.getElementById(type+"_mail"+i).innerHTML = mails[i];
            document.getElementById(type+"_mail"+i).style.display = "block";
        }
        else
        {
            document.getElementById(type+"_mail"+i).href = "";
            document.getElementById(type+"_mail"+i).innerHTML = "";
            document.getElementById(type+"_mail"+i).style.display = "none";
        }
    } 
   
    document.getElementById(type+"_twi").href = "https://twitter.com/" + item.twitter;
    document.getElementById(type+"_twi").innerHTML = "@"+ item.twitter;

    if (item.commission_permanente == "Sans commission")
        document.getElementById(type+"_commission").style.display = "none";
    else
        document.getElementById(type+"_commission").style.display = "inline";

    if (item.twitter == "")
        document.getElementById(type+"_twi").style.display = "none";
    else
        document.getElementById(type+"_twi").style.display = "inline";
}

function showContacts(button)
{
    var x = document.getElementById("contacts_coordo");
    if (x.style.display === "none")
    {
        x.style.display = "block";
        button.innerHTML = "Masquer les contacts";
    }
    else
    {
        x.style.display = "none";
        button.innerHTML = "Afficher les contacts";
    }
}
